package com.example.pawe.quizapp;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-09-23.
 */
@Parcel
public class Quiz {

    public ArrayList<Question> questions = new ArrayList<>();
    public int allQuestions;
    public int answersSoFar = 0;
    public int correctAnswers;

}
