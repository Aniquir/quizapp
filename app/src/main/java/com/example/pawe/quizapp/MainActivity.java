package com.example.pawe.quizapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textViewQuestion) TextView textViewQuestion;
    @BindView(R.id.RadioGroup) RadioGroup radioGroup;
    Quiz quiz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (getIntent().hasExtra("quiz")) {
            //odparcelowanie
            quiz = Parcels.unwrap(getIntent().getParcelableExtra("quiz"));
        } else {
            initQuiz();

        }
        setQuestion(quiz.questions.get(quiz.answersSoFar));
    }

    private void initQuiz() {
        //tworzenie pytania
        quiz = new Quiz();
        Question question = new Question(getString(R.string.question_one));
        question.answers.add(new Answer(getString(R.string.anwer_one), false));
        question.answers.add(new Answer(getString(R.string.anwer_two), false));
        question.answers.add(new Answer(getString(R.string.anwer_three), true));
        question.answers.add(new Answer(getString(R.string.anwer_four), false));
        quiz.questions.add(question);
        //dodanie drugiego pytania
        question = new Question(getString(R.string.question_two));
        question.answers.add(new Answer(getString(R.string.anwer2_one), false));
        question.answers.add(new Answer(getString(R.string.anwer2_two), false));
        question.answers.add(new Answer(getString(R.string.anwer2_three), false));
        question.answers.add(new Answer(getString(R.string.anwer2_four), true));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_three));
        question.answers.add(new Answer(getString(R.string.anwer3_one), false));
        question.answers.add(new Answer(getString(R.string.anwer3_two), true));
        question.answers.add(new Answer(getString(R.string.anwer3_three), false));
        question.answers.add(new Answer(getString(R.string.anwer3_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_four));
        question.answers.add(new Answer(getString(R.string.anwer4_one), false));
        question.answers.add(new Answer(getString(R.string.anwer4_two), true));
        question.answers.add(new Answer(getString(R.string.anwer4_three), false));
        question.answers.add(new Answer(getString(R.string.anwer4_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_five));
        question.answers.add(new Answer(getString(R.string.anwer5_one), false));
        question.answers.add(new Answer(getString(R.string.anwer5_two), false));
        question.answers.add(new Answer(getString(R.string.anwer5_three), true));
        question.answers.add(new Answer(getString(R.string.anwer5_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_six));
        question.answers.add(new Answer(getString(R.string.anwer6_one), false));
        question.answers.add(new Answer(getString(R.string.anwer6_two), false));
        question.answers.add(new Answer(getString(R.string.anwer6_three), false));
        question.answers.add(new Answer(getString(R.string.anwer6_four), true));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_seven));
        question.answers.add(new Answer(getString(R.string.anwer7_one), false));
        question.answers.add(new Answer(getString(R.string.anwer7_two), true));
        question.answers.add(new Answer(getString(R.string.anwer7_three), false));
        question.answers.add(new Answer(getString(R.string.anwer7_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_eight));
        question.answers.add(new Answer(getString(R.string.anwer8_one), true));
        question.answers.add(new Answer(getString(R.string.anwer8_two), false));
        question.answers.add(new Answer(getString(R.string.anwer8_three), false));
        question.answers.add(new Answer(getString(R.string.anwer8_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_nine));
        question.answers.add(new Answer(getString(R.string.anwer9_one), false));
        question.answers.add(new Answer(getString(R.string.anwer9_two), true));
        question.answers.add(new Answer(getString(R.string.anwer9_three), false));
        question.answers.add(new Answer(getString(R.string.anwer9_four), false));
        quiz.questions.add(question);

        question = new Question(getString(R.string.question_ten));
        question.answers.add(new Answer(getString(R.string.anwer10_one), false));
        question.answers.add(new Answer(getString(R.string.anwer10_two), true));
        question.answers.add(new Answer(getString(R.string.anwer10_three), false));
        question.answers.add(new Answer(getString(R.string.anwer10_four), false));
        quiz.questions.add(question);
    }

    private void setQuestion(Question question) {
        textViewQuestion.setText(question.text);
        for (Answer answer : question.answers) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(answer.text);
            radioGroup.addView(radioButton);
        }
    }

    private int getSelectedIndex() {

        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        return radioGroup.indexOfChild(radioButton);
    }

        @OnClick(R.id.ButtonNext) public void onClickNext() {
            if (getSelectedIndex() >= 0) {
                if (quiz.questions.get(quiz.answersSoFar).answers.get(getSelectedIndex()).isCorrect) {
                    Toast.makeText(this, "Poprawna odpowiedź!", Toast.LENGTH_SHORT).show();
                    //zwiekszamy liczbe poprawnych odpowiedzi
                    quiz.correctAnswers++;
                } else {
                    Toast.makeText(this, "Błędna odpowiedź", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Nie wybrano odpowiedzi", Toast.LENGTH_LONG).show();
                return;
            }

            Intent intent;
            if (quiz.answersSoFar >= 9) {
                intent = new Intent(this, HighScoreActivity.class);
            } else {
                intent = new Intent(this, MainActivity.class);
            }
                //zwiekszamy liczbe naciskania przycisku next
            quiz.answersSoFar++;
            //przekazywanie do nastepnego activity, przechodzenie do tego, nowe pytanie bedzie
            //musi byc w Parcels.wrap ze wzgledu na to, ze inaczej nie widzi
            intent.putExtra("quiz", Parcels.wrap(quiz));
            startActivity(intent);
            this.finish();

    }
}
