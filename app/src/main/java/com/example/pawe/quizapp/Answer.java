package com.example.pawe.quizapp;

import org.parceler.Parcel;

/**
 * Created by Paweł on 2017-09-23.
 */
@Parcel
class Answer {

    public String text;
    public boolean isCorrect;

    public Answer() {
    }

    public Answer(String text, boolean isCorrect) {
        this.text = text;
        this.isCorrect = isCorrect;
    }
}
