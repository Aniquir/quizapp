package com.example.pawe.quizapp;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-09-23.
 */
@Parcel
class Question {

    public String text;
    public ArrayList<Answer> answers = new ArrayList<>();

    public Question() {
    }
//poprawione, bo nie wyswietlalo imion
    public Question(String string) {
        this.text = string;
    }

    public Question(ArrayList<Answer> answers) {
        this.answers = answers;
    }
}
